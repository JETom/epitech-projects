#ifndef		DANONE_H__
#define		DANONE_H__

#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<string.h>
#include	<math.h>
#include	<time.h>

#define		MS_SEC			1000
#define		MS_MIN			60000
#define		MS_HOUR			3600000
#define		MS_DAY			86400000
#define		MS_MTH			2622600000
#define		MS_YR			31471200000
#define		NBR_VALUES		10
#define		V_MIN			0
#define		V_MAX			1000
#define		RAND_BETWEEN(a,b)	(rand() % (b - (a)) + a)
#define		USAGE			"Usage: %s [1,2]\n"
#define		ERR_OPTION		"Erreur: l'option '%s' n'existe pas.\n"


typedef enum e_bool {
  false,
  true
} bool;

typedef enum e_type {
  NORMAL,
  BEST,
  WORST
} t_type;

extern	bool	isGraphic;
extern	t_type	isSpecial;

int		*generate_vtab(int *vtab);
int		*generate_n_vtab(int *vtab, int n);
void		display_vtab(int *vtab);
void		selection_sort(int *values, int nbr_values);
void		insertion_sort(int *values, int nbr_values);
void		bubble_sort(int *values, int nbr_values);
void		quick_sort(int *values, int nbr_values);
void		merge_sort(int *values, int nbr_values);
long		get_log_cplx(long a10, long a20);
long		get_exp_cplx(long a10, long a20);
void		option_three(long elem);

#endif		/* DANONE_H__ */
