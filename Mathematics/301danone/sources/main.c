#include "301danone.h"

bool	isGraphic = false;
t_type	isSpecial = NORMAL;

void	parse_arg(char *arg)
{
  if (!strcmp(arg, "--graphic"))
    isGraphic = true;
  if (!strcmp(arg, "--best"))
    isSpecial = BEST;
  if (!strcmp(arg, "--worst"))
    isSpecial = WORST;
}

int	main(int ac, char **av)
{
  srand(time(NULL));
  if (ac < 2)
    {
      printf(USAGE, av[0]);
      return (EXIT_FAILURE);
    }
  int i;
  for (i = 1; i < ac; i++)
    parse_arg(av[i]);
  if (!strcmp(av[1], "1"))
    option_one();
  else if (!strcmp(av[1], "2"))
    option_two();
  else if (!strcmp(av[1], "3") && ac >= 3)
    option_three(atol(av[2]));
  else
    {
      printf(ERR_OPTION, av[1]);
      return (EXIT_FAILURE);
    }
  return (EXIT_SUCCESS);
}
