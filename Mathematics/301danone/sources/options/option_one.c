#include "301danone.h"

void	option_one()
{
  int	vtab[NBR_VALUES];

  puts("tri rapide");
  generate_vtab(vtab);
  display_vtab(vtab);
  quick_sort(vtab, NBR_VALUES);
  display_vtab(vtab);

  puts("tri fusion");
  generate_vtab(vtab);
  display_vtab(vtab);
  merge_sort(vtab, NBR_VALUES);
  display_vtab(vtab);

  puts("tri à bulles");
  generate_vtab(vtab);
  display_vtab(vtab);
  bubble_sort(vtab, NBR_VALUES);
  display_vtab(vtab);

  puts("tri par sélection");
  generate_vtab(vtab);
  display_vtab(vtab);
  selection_sort(vtab, NBR_VALUES);
  display_vtab(vtab);

  puts("tri par insertion");
  generate_vtab(vtab);
  display_vtab(vtab);
  insertion_sort(vtab, NBR_VALUES);
  display_vtab(vtab);
}
