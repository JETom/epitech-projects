#include "301danone.h"

char	*format_time(long msec)
{
  char *s;

  s = malloc(128);
  if (msec / MS_YR > 0)
    {
      sprintf(s, "%ld an(s)\t", msec / MS_YR);
      return (s);
    }
  /* if (msec / MS_MTH > 0) */
  /*   { */
  /*     sprintf(s, "%ld mois\t", msec / MS_MTH); */
  /*     return (s); */
  /*   } */
  if (msec / MS_DAY > 0)
    {
      sprintf(s, "%ld j\t", msec / MS_DAY);
      return (s);
    }
  if (msec / MS_HOUR > 0)
    {
      sprintf(s, "%ld hr\t", msec / MS_HOUR);
      return (s);
    }
  if (msec / MS_MIN > 0)
    {
      sprintf(s, "%ld min\t", msec / MS_MIN);
      return (s);
    }
  if (msec / MS_SEC > 0)
    {
      sprintf(s, "%ld sec\t", msec / MS_SEC);
      return (s);
    }
  sprintf(s, "%ld ms\t", msec);
  return (s);
}

long	get_timediff(struct timeval *start_time,
		     struct timeval *end_time,
		     struct timeval *diff_time)
{
  diff_time->tv_sec = end_time->tv_sec - start_time->tv_sec;
  diff_time->tv_usec = end_time->tv_usec - start_time->tv_usec;
  while(diff_time->tv_usec < 0)
    {
      diff_time->tv_usec += 1000000;
      diff_time->tv_sec -= 1;
    }
  return (diff_time->tv_sec * 1000000 + diff_time->tv_usec);
}

long	do_sort(char *sortname, void (*sort_func)(int *, int), int **vtabs, int len)
{
  struct timeval start_time, end_time, diff_time;
  long times[6];
  int *tmp;
  int i;

  printf("%s", sortname);
  tmp = malloc(sizeof(int) * len);
  for (i = 0; i < 6; i++)
    {
      memcpy(tmp, vtabs[i], sizeof(int) * len);
      gettimeofday(&start_time, NULL);
      sort_func(tmp, len);
      gettimeofday(&end_time, NULL);
      times[i] = get_timediff(&start_time, &end_time, &diff_time) / 1000;
      printf("\t%ld", times[i]);
    }
  printf("\n");
  free(tmp);
  long avr = 0;
  for (i = 0; i < 6; i++)
    avr += times[i];
  return (avr / 6);
}

void	option_two()
{
 int **vtabs;
 int i;
 long avr10[5];
 long avr20[5];
 vtabs = malloc(sizeof(int *) * 6);

 for (i = 0; i < 6; i++)
   vtabs[i] = generate_n_vtab(vtabs[i], 10000);
 printf("10 000 éléments :\n");
 avr10[0] = do_sort("tri rapide       ", &quick_sort, vtabs, 10000);
 avr10[1] = do_sort("tri fusion       ", &merge_sort, vtabs, 10000);
 avr10[2] = do_sort("tri à bulles     ", &bubble_sort, vtabs, 10000);
 avr10[3] = do_sort("tri par sélection", &selection_sort, vtabs, 10000);
 avr10[4] = do_sort("tri par insertion", &insertion_sort, vtabs, 10000);


 for (i = 0; i < 6; i++)
   {
     free(vtabs[i]);
     vtabs[i] = generate_n_vtab(vtabs[i], 20000);
   }
 printf("\n\n20 000 éléments :\n");
 avr20[0] = do_sort("tri rapide       ", &quick_sort, vtabs, 20000);
 avr20[1] = do_sort("tri fusion       ", &merge_sort, vtabs, 20000);
 avr20[2] = do_sort("tri à bulles     ", &bubble_sort, vtabs, 20000);
 avr20[3] = do_sort("tri par sélection", &selection_sort, vtabs, 20000);
 avr20[4] = do_sort("tri par insertion", &insertion_sort, vtabs, 20000);


 puts("\n");
 printf("%s\t%s\t%s\t%s\n", "                 ", "10 000 entiers", "20 000 entiers", "10^8 entiers");

 printf("%s\t%s\t%s\t%s\n", "tri rapide       ", format_time(avr10[0]), format_time(avr20[0]), format_time(get_log_cplx(avr10[0], avr20[0])));
 printf("%s\t%s\t%s\t%s\n", "tri fusion       ", format_time(avr10[1]), format_time(avr20[1]), format_time(get_log_cplx(avr10[1], avr20[1])));
 printf("%s\t%s\t%s\t%s\n", "tri à bulles     ", format_time(avr10[2]), format_time(avr20[2]), format_time(get_exp_cplx(avr10[2], avr20[2])));
 printf("%s\t%s\t%s\t%s\n", "tri par sélection", format_time(avr10[3]), format_time(avr20[3]), format_time(get_exp_cplx(avr10[3], avr20[3])));
 printf("%s\t%s\t%s\t%s\n", "tri par insertion", format_time(avr10[4]), format_time(avr20[4]), format_time(get_exp_cplx(avr10[4], avr20[4])));

 if (isGraphic == true)
   {
     char cmd[256];
     sprintf(cmd, "./python/graph %ld %ld %ld %ld %ld", 
	     avr20[0], avr20[1], avr20[2], avr20[3], avr20[4]);
     system(cmd);
   }
}



void	option_three(long elem)
{
 int **vtabs;
 int i;
 long avr[5];

 if (elem < 2)
   {
     printf("Erreur: le nombre d'éléments est trop faible (%ld).\n", elem);
     exit(EXIT_FAILURE);
   }
 vtabs = malloc(sizeof(int *) * 6);

 for (i = 0; i < 6; i++)
   vtabs[i] = generate_n_vtab(vtabs[i], elem);

 printf("Estimations durées de traitements dans le pire des cas :\n");
 printf("Tri rapide et tri fusion             : %s\n", format_time(calc_nlogn(elem)));
 printf("Tri à bulles, sélection et insertion : %s\n", format_time(calc_nexp(elem)));
 puts("\n");
 printf("%ld éléments :\n", elem);
 avr[0] = do_sort("tri rapide       ", &quick_sort, vtabs, elem);
 avr[1] = do_sort("tri fusion       ", &merge_sort, vtabs, elem);
 avr[2] = do_sort("tri à bulles     ", &bubble_sort, vtabs, elem);
 avr[3] = do_sort("tri par sélection", &selection_sort, vtabs, elem);
 avr[4] = do_sort("tri par insertion", &insertion_sort, vtabs, elem);

 puts("\nDurées de traitement :");

 printf("%s\t%s\n", "tri rapide       ", format_time(avr[0]));
 printf("%s\t%s\n", "tri fusion       ", format_time(avr[1]));
 printf("%s\t%s\n", "tri à bulles     ", format_time(avr[2]));
 printf("%s\t%s\n", "tri par sélection", format_time(avr[3]));
 printf("%s\t%s\n", "tri par insertion", format_time(avr[4]));

 if (isGraphic == true)
   {
     char cmd[256];
     sprintf(cmd, "./python/graph %ld %ld %ld %ld %ld", 
	     avr[0], avr[1], avr[2], avr[3], avr[4]);
     system(cmd);
   }
 free(vtabs);
}
