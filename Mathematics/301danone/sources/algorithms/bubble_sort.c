#include "301danone.h"

void	bubble_sort(int *values, int nbr_values)
{
  bool changed;
  int n = nbr_values;
  do {
    changed = false;
    int j;
    for (j = 0; j < n - 1; j++)
      {
	if (values[j] > values[j + 1])
	  {
	    int tmp = values[j];
	    values[j] = values[j + 1];
	    values[j + 1] = tmp;
	    changed = true;
	  }
      }
    n--;
  } while (changed == true);
}
