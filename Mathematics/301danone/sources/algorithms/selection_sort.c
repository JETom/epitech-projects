#include "301danone.h"

static void	swap(int *a, int *b)
{
  int		tmp;

  tmp = *a;
  *a = *b;
  *b = tmp;
}

void		selection_sort(int *values, int nbr_values)
{
  int		i, j;
  int		min;

  i = 0;
  while (i < nbr_values - 1)
    {
      min = i;
      j = min + 1;
      while (j < nbr_values)
	{
	  if (values[j] < values[min])
	    min = j;
	  j++;
	}
      swap(&values[i], &values[min]);
      i++;
    }
}
