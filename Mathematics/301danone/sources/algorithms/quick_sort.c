#include "301danone.h"

static void	swap(int *a, int *b)
{
  int		tmp;

  tmp = *a;
  *a = *b;
  *b = tmp;
}

static void	shuffle(int *values, int nbr_values)
{
  int		i;

  i = 0;
  while (i < nbr_values - 1)
    swap(&values[i++], &values[RAND_BETWEEN(0, nbr_values)]);
}

static int	partition(int *array, int lo, int hi)
{
  int		pivot = array[(lo + hi) / 2];

  while (lo <= hi)
    {
      while (array[lo] < pivot) lo++;
      while (array[hi] > pivot) hi--;
      if (lo <= hi)
	swap(&array[lo++], &array[hi--]);
    }
  return (lo);
}

static void	sort(int *array, int lo, int hi)
{
  int		middle;

  if (lo >= hi)
    return;
  middle = partition(array, lo, hi);
  if (lo < middle - 1)
    sort(array, lo, middle - 1);
  if (middle < hi)
    sort(array, middle, hi);
}

void		quick_sort(int *values, int nbr_values)
{
  shuffle(values, nbr_values);
  sort(values, 0, nbr_values - 1);
}
