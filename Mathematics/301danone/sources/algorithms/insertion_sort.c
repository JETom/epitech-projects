#include "301danone.h"

void	insertion_sort(int *values, int nbr_values)
{
  int i;
  for (i = 0; i < nbr_values - 1; i++)
    {
      int min = i;
      int j;
      for (j = i + 1; j < nbr_values; j++)
	{
	  if (values[j] < values[min])
	    min = j;
	}
      if (min != i)
	{
	  int tmp = values[i];
	  values[i] = values[min]; 
	  values[min] = tmp;
	}
    }
}
