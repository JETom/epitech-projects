#include "301danone.h"

void	merge(int *values, int first1, int last1, int last2, int nbr_values)
{
  int	*vdup;
  int	i;
  int	first2 = last1 + 1;
  int	cpt1 = first1;
  int	cpt2 = first2;

  vdup = malloc(sizeof(int) * (last1 - first1 + 1));
  memcpy(vdup, values ,sizeof(int) * (last1 - first1 + 1));
  for (i = first1; i <= last1; i++)
    vdup[i - first1] = values[i];

  for (i = first1; i <= last2; i++)
    {
      if (cpt1 == first2)
	break;
      else if (cpt2 == last2 + 1)
	{
	  values[i] = vdup[cpt1 - first1];
	  cpt1++;
	}
      else if (vdup[cpt1 - first1] < values[cpt2])
	{
	  values[i] = vdup[cpt1 - first1];
	  cpt1++;
	}
      else
	{
	  values[i] = values[cpt2];
	  cpt2++;
	}
    }
  free(vdup);
}

void	recursive_merge(int *values, int first, int last, int nbr_values)
{
  if (first != last)
    {
      int mid = (first + last) / 2;
      recursive_merge(values, first, mid, nbr_values);
      recursive_merge(values, mid + 1, last, nbr_values);
      merge(values, first, mid, last, nbr_values);
    }
}

void	merge_sort(int *values, int nbr_values)
{
  recursive_merge(values, 0, nbr_values - 1, nbr_values);
}
