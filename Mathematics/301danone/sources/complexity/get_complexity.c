#include "301danone.h"

double coeff_direct(long xa, long ya, long xb, long yb)
{
  return ((yb - ya) / (xb - xa));
}

long val_origin(double dir, long x, long y)
{
  return (y - dir * x);
}

long calc_nlogn(long n)
{
  return (n * logl(n));
}

long calc_nexp(long n)
{
  return (powl(n, 2));
}

long rev_exp(int n, long y)
{
  return (sqrtl(y / n));
}

long rev_log(int n, long y)
{
  return (expl(y / n));
}

long get_log_cplx(long a10, long a20)
{
  /* long approx1 = a10 * calc_nlogn(100000000) / calc_nlogn(10000); */
  long approx2 = a20 * calc_nlogn(100000000) / calc_nlogn(20000);
  double app = a20 * (calc_nlogn(10000) * 1.0) / calc_nlogn(20000);
  return (approx2);
  /* return ((approx1 + approx2) / 2); */
}

long get_exp_cplx(long a10, long a20)
{
  long approx1 = a10 * calc_nexp(100000000) / calc_nexp(10000);
  return (approx1);
  /* long approx2 = a20 * calc_nexp(100000000) / calc_nexp(20000); */
  /* return ((approx1 + approx2) / 2); */
}
