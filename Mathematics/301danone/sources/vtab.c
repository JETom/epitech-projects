#include "301danone.h"

int	*generate_vtab(int *vtab)
{
  int	i;

  if (isSpecial == BEST) {
    for (i = 0; i < NBR_VALUES; i++)
      vtab[i] = i;
  }
  else if (isSpecial == WORST) {
    for (i = 0; i < NBR_VALUES; i++)
      vtab[i] = NBR_VALUES - i;    
  }
  else {
    for (i = 0; i < NBR_VALUES; i++)
      vtab[i] = V_MIN + rand() % (V_MAX - V_MIN + 1);
  } 
  return (vtab);
}

int	*generate_n_vtab(int *vtab, int n)
{
  int	i;

  if ((vtab = malloc(sizeof(int) * n)) == NULL)
    exit(EXIT_FAILURE);
  if (isSpecial == BEST) {
    for (i = 0; i < n; i++)
      vtab[i] = i;    
  }
  else if (isSpecial == WORST) {
    for (i = 0; i < n; i++)
      vtab[i] = n - i;    
  }
  else {
    for (i = 0; i < n; i++)
      vtab[i] = rand() % (n + 1);
  } 
  
return (vtab);
}

void	display_vtab(int *vtab)
{
  int i;

  printf("\t");
  for (i = 0; i < 10; i++)
    printf("%d\t", vtab[i]);
  printf("\n");
}

void	display_n_vtab(int *vtab, int n)
{
  int i;

  printf("\t");
  for (i = 0; i < n; i++)
    printf("%d\t", vtab[i]);
  printf("\n");
}
